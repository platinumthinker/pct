#include <mpi.h>
#include <stdio.h>

#define tagFloatData 1
#define tagDoubleData 2

int main(int argc, char **argv)
{
    int size, rank, count, i;
    double t1, t2;
    float floatData[10];
    double doubleData[20];
    MPI_Status status;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if(size != 2)
    {
        if(rank == 0)
        {
            printf("Error: two processes reqired instead of %d, abort\n", size);
        }
        MPI_Barrier(MPI_COMM_WORLD);        
        MPI_Abort(MPI_COMM_WORLD, MPI_ERR_OTHER);

        return -1;
    }
    
    if(rank == 0)
    {
        for(i = 0; i < 10; i++)
        {
            floatData[i] = i;
        }
        for(i = 0; i < 20; i++)
        {
            doubleData[i] = -i;
        }
        t1 = MPI_Wtime();
        MPI_Send(floatData,9 , MPI_FLOAT, 1, tagFloatData, MPI_COMM_WORLD);
        t2 = MPI_Wtime()-t1;
        printf("time sent float = %f\n", t2);
        t1 = MPI_Wtime();
        MPI_Send(doubleData, 19, MPI_DOUBLE, 1, tagDoubleData, MPI_COMM_WORLD);
        t2 = MPI_Wtime() - t1;
        printf("time send double = %f\n", t2);
     }
     else
     {
        t1 = MPI_Wtime();
        MPI_Recv(floatData, 10, MPI_FLOAT, 0, tagFloatData, MPI_COMM_WORLD, &status);
        t2 = MPI_Wtime() - t1;
        printf("time received float = %f\n", t2);

        MPI_Get_count(&status, MPI_FLOAT, &count);
        for(i = 0; i < count; i++)
        {
            if(floatData[i] != i)
            {
                printf("Error 1: i = %3d\n", i);
            }
        }
        printf("rank = %d Received %d elems float\n", rank, count);
        t1 = MPI_Wtime();
        MPI_Recv(doubleData, 20, MPI_DOUBLE, 0, tagDoubleData, MPI_COMM_WORLD, &status);
        t2 = MPI_Wtime() - t1;
        printf("time received double = %f\n", t2);
        MPI_Get_count(&status, MPI_DOUBLE, &count);
        for(i = 0; i < count; i++)
        {
            if(doubleData[i] != -i)
            {
                printf("Error 2: i = %3d\n", i);
            }
        }
        printf("Received %d elems double\n", count);
     }
     MPI_Barrier(MPI_COMM_WORLD);
     MPI_Finalize();
     return 0;
}
